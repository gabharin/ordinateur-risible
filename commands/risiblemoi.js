const { SlashCommandBuilder } = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("risiblemoi")
    .setDescription("Renvoie un Risitas"),
  async execute(interaction) {
    let req = await fetch("https://risibank.fr/api/v1/medias/random", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    let res = await req.json();
    let risitas;
    for (let foundRisitas of res) {
      if (foundRisitas.source_url && !risitas) {
        risitas = foundRisitas;
      }
    }
    await interaction.reply(risitas.source_url);
  },
};
