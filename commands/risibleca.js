const { SlashCommandBuilder } = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("risibleca")
    .setDescription("Recherche un Risitas")
    .addStringOption((option) =>
      option
        .setName("recherche")
        .setDescription("La recherche voulue pour le Risitas")
        .setRequired(true)
    ),
  async execute(interaction) {
    const search = interaction.options.getString("recherche") ?? "test";
    let req = await fetch(
      `https://risibank.fr/api/v1/medias/search?query=${search}&page=1`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    let res = await req.json();
    let listOfRisitas = res.medias;
    let risitas =
      listOfRisitas[Math.floor(Math.random() * listOfRisitas.length)];
    await interaction.reply(risitas.source_url);
  },
};
